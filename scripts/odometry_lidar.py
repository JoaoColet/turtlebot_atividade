#!/usr/bin/env python3

import rospy
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion
from sensor_msgs.msg import LaserScan
import numpy as np
import time

EPS = 0.0001
MAX_ITER = 100

#ICP starts here

def icp_matching(previous_points, current_points):
    """
    Iterative Closest Point matching
    - input
    previous_points: 2D or 3D points in the previous frame
    current_points: 2D or 3D points in the current frame
    - output
    R: Rotation matrix
    T: Translation vector
    """
    H = None  # homogeneous transformation matrix

    dError = np.inf
    preError = np.inf
    count = 0

    while dError >= EPS:
        count += 1

        indexes, error = nearest_neighbor_association(previous_points, current_points)
        Rt, Tt = svd_motion_estimation(previous_points[:, indexes], current_points)
        # update current points
        current_points = (Rt @ current_points) + Tt[:, np.newaxis]

        dError = preError - error
        #print("Residual:", error)

        if dError < 0:  # prevent matrix H changing, exit loop
            #print("Not Converge...", preError, dError, count)
            break

        preError = error
        H = update_homogeneous_matrix(H, Rt, Tt)

        if dError <= EPS:
            #print("Converge", error, dError, count)
            break
        elif MAX_ITER <= count:
            #print("Not Converge...", error, dError, count)
            break

    R = np.array(H[0:-1, 0:-1])
    T = np.array(H[0:-1, -1])

    return R, T

def update_homogeneous_matrix(Hin, R, T):

    r_size = R.shape[0]
    H = np.zeros((r_size + 1, r_size + 1))

    H[0:r_size, 0:r_size] = R
    H[0:r_size, r_size] = T
    H[r_size, r_size] = 1.0

    if Hin is None:
        return H
    else:
        return Hin @ H

def nearest_neighbor_association(previous_points, current_points):

    # calc the sum of residual errors
    delta_points = previous_points - current_points
    d = np.linalg.norm(delta_points, axis=0)
    error = sum(d)

    # calc index with nearest neighbor assosiation
    d = np.linalg.norm(np.repeat(current_points, previous_points.shape[1], axis=1) - np.tile(previous_points, (1, current_points.shape[1])), axis=0)
    indexes = np.argmin(d.reshape(current_points.shape[1], previous_points.shape[1]), axis=1)

    return indexes, error

def svd_motion_estimation(previous_points, current_points):
    pm = np.mean(previous_points, axis=1)
    cm = np.mean(current_points, axis=1)

    p_shift = previous_points - pm[:, np.newaxis]
    c_shift = current_points - cm[:, np.newaxis]

    W = c_shift @ p_shift.T
    u, s, vh = np.linalg.svd(W)

    R = (u @ vh).T
    t = pm - (R @ cm)

    return R, t

class TurtleControl:
    def __init__(self):
        rospy.init_node("tb3control_node", anonymous=True)
        self.vel_publisher = rospy.Publisher("tb3_0/cmd_vel", Twist, queue_size=10)
        rospy.Subscriber("tb3_0/scan", LaserScan, self.update_scan)
        rospy.Subscriber("tb3_0/odom", Odometry, self.update_pose)
        self.rate = rospy.Rate(10)
        self.pose = Pose()
        self.pose_lidar = Pose()
        self.ref_pose = Pose()
        self.max_vel = 0.22
        self.max_ang = 2.84
        self.scan = LaserScan()
        self.scanant = LaserScan()        

    def update_pose(self, msg):
        orientation_q = msg.pose.pose.orientation
        orientation_list = [orientation_q.x, orientation_q.y, orientation_q.z, orientation_q.w]
        (_, _, yaw) = euler_from_quaternion(orientation_list)
        self.pose.x = msg.pose.pose.position.x
        self.pose.y = msg.pose.pose.position.y
        self.pose.theta =  yaw

    def update_scan(self, msg):
    	self.scan = msg

    def ref_distance(self):
        return np.sqrt(  (self.ref_pose.x - self.pose_lidar.x)**2 + (self.ref_pose.y - self.pose_lidar.y)**2)

    def linear_vel_control(self, kp = 1.5):
        distance = self.ref_distance()
        control = kp* distance
        if abs(control) > self.max_vel:
            control = self.max_vel*np.sign(control)
        return control

    def angular_vel_control(self, kp=6):
        angle_r = np.arctan2(self.ref_pose.y - self.pose.y,  self.ref_pose.x - self.pose.x )        
        control = kp*(angle_r - self.pose.theta)
        if abs(control) > self.max_ang:
            control = self.max_ang*np.sign(control)
        return control



    def Laser2icp(self):

        motion = [self.pose_lidar.x, self.pose_lidar.y, self.pose_lidar.theta]

        self.scanant = self.scan
        time.sleep(4)
        previous_points = np.zeros((2, 360))
        current_points = np.zeros((2, 360))
        if motion[2] < 0:
            motion[2] += 2*np.pi
        for i in range(360):
            prev = 0
            if self.scanant.ranges[i] <= 3.5:
                prev = self.scanant.ranges[i]
            else:
                prev = 3.5
            previous_points[0][i] = motion[0] + (prev * np.cos(motion[2] + np.deg2rad(i)))
            previous_points[1][i] = motion[1] + (prev * np.sin(motion[2] + np.deg2rad(i)))
            curr = 0
            if self.scan.ranges[i] <= 3.5:
                curr = self.scan.ranges[i]
            else:
                curr = 3.5
            current_points[0][i] = motion[0] + (curr * np.cos(motion[2] + np.deg2rad(i)))
            current_points[1][i] = motion[1] + (curr * np.sin(motion[2] + np.deg2rad(i)))
        
                
        R, T = icp_matching(previous_points, current_points)

        ang = np.arccos(R[0][0])
        self.pose_lidar.theta = self.pose_lidar.theta + ang
        if self.pose_lidar.theta >= 2*np.pi:
            self.pose_lidar.theta -= 2*np.pi
        self.pose_lidar.x = self.pose_lidar.x + float(T[0])
        self.pose_lidar.y = self.pose_lidar.y + float(T[1])
        rospy.loginfo("X = ")
        rospy.loginfo(self.pose_lidar.x)
        rospy.loginfo("Y = ")
        rospy.loginfo(self.pose_lidar.y)
        rospy.loginfo("theta = ")
        rospy.loginfo(self.pose_lidar.theta)

    def move2ref(self):
        ref_tol = 0.05
        vel_msg = Twist()
        self.pose_lidar.x = 0
        self.pose_lidar.y = 0
        self.pose_lidar.theta = 0
        while not rospy.is_shutdown():
            rospy.loginfo("Ref X = ")
            self.ref_pose.x = float(input())
            rospy.loginfo("Ref Y = ")
            self.ref_pose.y = float(input())
            vel_msg.linear.y = 0
            vel_msg.linear.z = 0
            vel_msg.angular.x = 0
            vel_msg.angular.y = 0
            while self.ref_distance() >= ref_tol:
                self.Laser2icp()
                vel_msg.linear.x = self.linear_vel_control()
                vel_msg.angular.z = self.angular_vel_control()
                self.vel_publisher.publish(vel_msg)
                self.rate.sleep()

            # stop
            vel_msg.linear.x = 0
            vel_msg.angular.z= 0
            self.vel_publisher.publish(vel_msg)
            rospy.loginfo("Chegou!!!")
            rospy.loginfo(self.pose.x)
            rospy.loginfo(self.pose.y)
            

if __name__ == '__main__':
    bot = TurtleControl()
    time.sleep(5)
    rospy.loginfo("Press Enter to start")
    trash = input()
    bot.move2ref()    

